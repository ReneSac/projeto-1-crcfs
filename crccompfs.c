/*
 * O compfs é baseado no goislenefs. Mantive os comentários e nomes de funções. Só 
 * mudei a identificação que o sistema de arquivos faz ao kernel, e adicionei
 * a função de checkar CRC.
 *
 * goislenefs: sistema de arquivos de brinquedo para utilização em sala de aula.
 * Versão original escrita por Glauber de Oliveira Costa para a disciplina MC514
 * (Sistemas Operacionais: teoria e prática) no primeiro semestre de 2008.
 * Código atualizado para rodar com kernel 3.10.x.
 */
 
#include <linux/module.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/vfs.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/mm.h>
#include <linux/crc32c.h> /* vai calcular nosso CRC */

#include <asm/current.h>
#include <asm/uaccess.h>


/* Coisas que o LZO depende */ 

#include <linux/pagemap.h>
#include <linux/bio.h>
#include <linux/lzo.h>

#define LZO_LEN	4

struct workspace {
	void *mem;
	void *buf;	/* where compressed data goes */
	void *cbuf;	/* where decompressed data goes */
};

static void lzo_free_workspace(struct workspace *workspace)
{
	vfree(workspace->buf);
	vfree(workspace->cbuf);
	vfree(workspace->mem);
	kfree(workspace);
}

static struct workspace *lzo_alloc_workspace(void)
{
	struct workspace *workspace;

	workspace = kzalloc(sizeof(*workspace), GFP_NOFS);
	if (!workspace)
		return ERR_PTR(-ENOMEM);

	workspace->mem = vmalloc(LZO1X_MEM_COMPRESS);
	workspace->buf = vmalloc(lzo1x_worst_compress(PAGE_CACHE_SIZE));
	workspace->cbuf = vmalloc(lzo1x_worst_compress(PAGE_CACHE_SIZE));
	if (!workspace->mem || !workspace->buf || !workspace->cbuf)
		goto fail;


	return workspace;
fail:
	lzo_free_workspace(workspace);
	return ERR_PTR(-ENOMEM);
}






/* O compfs eh um fs muito simples. Os inodes sao atribuidos em ordem crescente, sem
 * reaproveitamento */
static int inode_number = 0;

/* Lembre-se que nao temos um disco! (Isso so complicaria as coisas, pois teriamos
 * que lidar com o sub-sistema de I/O. Entao teremos uma representacao bastante
 * simples da estrutura de arquivos: Uma lista duplamente ligada circular (para
 * aplicacoes reais, um hash seria muito mais adequado) contem em cada elemento
 * um indice (inode) e uma pagina para conteudo (ou seja: o tamanho maximo de um
 * arquivo nessa versao do islene fs eh de 4Kb. Nao ha subdiretorios */
struct file_contents {
	struct list_head list;
	struct inode *inode;
	u32 crc; /* Adicionado um campo de CRC ao conteúdo do arquivo. */
	size_t unc_size;
	size_t cmp_size;
	void *conts;
};

/* lista duplamente ligada circular, contendo todos os arquivos do fs */
static LIST_HEAD(contents_list);

static const struct super_operations compfs_ops = {
        .statfs         = simple_statfs,
        .drop_inode     = generic_delete_inode,
};

/* Lembram quando eu disse que um hash seria mais eficiente? ;-) */
static struct file_contents *compfs_find_file(struct inode *inode)
{
	struct file_contents *f;
	list_for_each_entry(f, &contents_list, list) {
		if (f->inode == inode)
			return f;
	}
	return NULL;
}




int compfs_decompress_file(struct file_contents *f, unsigned char *data_out, 
		size_t *out_len) {
	int ret = 0;
	data_out = vmalloc(f->unc_size + 16);
		
	ret = lzo1x_decompress_safe(f->conts, f->cmp_size, data_out, out_len);
				       
	return ret;
}

	
int compfs_compress_file(struct file_contents *f, const unsigned char *data_in, 
		size_t in_len) {
	int ret = 0;
	size_t out_len;
	
	struct workspace *workspace = lzo_alloc_workspace();
	if (IS_ERR(workspace))
		return -1;

	ret = lzo1x_1_compress(data_in, in_len, workspace->cbuf,
				       &out_len, workspace->mem);
				       
	/* Copia comprimido pra o arquivo. */	
	memcpy(f->conts, workspace->cbuf, out_len);
	
	f->unc_size = in_len;
	f->cmp_size = out_len;
	
	printk(KERN_INFO "unc_size: %d, cmp_size: %d", in_len, out_len);
				       
	lzo_free_workspace(workspace);
	return ret;
}




/* Apos passar pelo VFS, uma leitura chegara aqui. A unica
 * coisa que fazemos eh, achar o ponteiro para o conteudo do arquivo,
 * e retornar, de acordo com o tamanho solicitado */
ssize_t compfs_read(struct file *file, char __user *buf,
		      size_t count, loff_t *pos)
{
	struct file_contents *f;
	struct inode *inode = file->f_path.dentry->d_inode;
	int size = count;
	u32 current_crc;
	size_t out_len;
	unsigned char *data_out; /* Espaço seguro. */

	f = compfs_find_file(inode);
	if (f == NULL)
		return -EIO;
		
	if (f->inode->i_size < count)
		size = f->inode->i_size;

	if ((*pos + size) >= f->inode->i_size)
		size = f->inode->i_size - *pos;
		
	/* Verifica se o CRC atual confere com o arquivado. Se não conferir,
	 * abortar a operação de leitura. */
	current_crc = crc32c(0, f->conts, f->inode->i_size);
	if (current_crc != f->crc) {
		printk(KERN_INFO "Problema! O CRC não bate!"
			" O arquivo foi alterado desde sua criação.\n"
			"CRC atual: %#8x\nCRC guardada: %#8x\n", 
			current_crc, f->crc);
		return -EIO;
	} else {
		printk(KERN_INFO "Tudo ok. CRC guardada: %#8x\n", f->crc);
	}
	
	
	
	compfs_decompress_file(f, data_out, &out_len);
	
	if (out_len != f->unc_size) {
		printk(KERN_INFO "out_len != f->unc_size: %d, %d", 
				out_len, f->unc_size);
	}
	
	/* As page tables do kernel estao sempre mapeadas (veremos o que
	 * sao page tables mais pra frente do curso), mas o mesmo nao eh
	 * verdade com as paginas de espaco de usuario. Dessa forma, uma
	 * atribuicao de/para um ponteiro contendo um endedereco de espaco
	 * de usuario pode falhar. Dessa forma, toda a comunicacao
	 * de/para espaco de usuario eh feita com as funcoes copy_from_user()
	 * e copy_to_user(). */
	if (copy_to_user(buf, data_out + *pos, size))
		return -EFAULT;
	*pos += size;

	return size;
}




/* similar a leitura, mas vamos escrever no ponteiro do conteudo.
 * Por simplicidade, estamos escrevendo sempre no comeco do arquivo.
 * Obviamente, esse nao eh o comportamento esperado de um write 'normal'
 * Mas implementacoes de sistemas de arquivos sao flexiveis... */
ssize_t compfs_write(struct file *file, const char __user *buf,
		       size_t count, loff_t *pos)
{
	struct file_contents *f;
	struct inode *inode = file->f_path.dentry->d_inode;
	size_t in_len = count + *pos;
	unsigned char *data_in = vmalloc(in_len + 16); /* Espaço seguro. */

	f = compfs_find_file(inode);
	if (f == NULL)
		return -ENOENT;
	
	/* Preparar buffers para compressão. */	
	/* copy_from_user() : veja comentario na funcao de leitura */
	if (copy_from_user(data_in + *pos, buf, count))
		return -EFAULT;
	
	compfs_compress_file(f, data_in, in_len);
		
	/* Registra o CRC do arquivo. Se o CRC for igual a 0, provavelmente é 
	 * a primeira escrita ao arquivo. */	
	if (f->crc == 0) {
		f->crc = crc32c(0, f->conts, count);
		printk(KERN_INFO "CRC arquivado. Comprimento = %d, CRC = %#8x",
			 count, f->crc);
	}
	inode->i_size = count;	

	return count;
}

static int compfs_open(struct inode *inode, struct file *file)
{
	/* Todo arquivo tem uma estrutura privada associada a ele.
	 * Em geral, estamos apenas copiando a do inode, se houver. Mas isso
	 * eh tao flexivel quanto se queira, e podemos armazenar aqui
	 * qualquer tipo de coisa que seja por-arquivo. Por exemplo: Poderiamos
	 * associar um arquivo /mydir/4321 com o processo no 4321 e guardar aqui
	 * a estrutura que descreve este processo */
	if (inode->i_private)
		file->private_data = inode->i_private;
	return 0;
}

const struct file_operations compfs_file_operations = {
	.read = compfs_read,
	.write = compfs_write,
	.open = compfs_open,
};

static const struct inode_operations compfs_file_inode_operations = {
	.getattr        = simple_getattr,
};

/* criacao de um arquivo: sem misterio, sem segredo, apenas
 * alocar as estruturas, preencher, e retornar */
static int compfs_create (struct inode *dir, struct dentry * dentry,
			    umode_t mode, bool excl)
{
	struct inode *inode;
	struct file_contents *file = kmalloc(sizeof(*file), GFP_KERNEL);	
	struct page *page;

	if (!file)
		return -EAGAIN;

	inode = new_inode(dir->i_sb);

	inode->i_mode = mode | S_IFREG;
	inode->i_uid = current->cred->fsuid;
	inode->i_gid = current->cred->fsgid;
	inode->i_blocks = 0;
	inode->i_atime = inode->i_mtime = inode->i_ctime = CURRENT_TIME;
        inode->i_ino = ++inode_number;

	inode->i_op = &compfs_file_inode_operations;
	inode->i_fop = &compfs_file_operations;

	file->inode = inode;
	file->crc = 0;
	page = alloc_page(GFP_KERNEL);
	if (!page)
		goto cleanup;

	file->conts = page_address(page);
	INIT_LIST_HEAD(&file->list);
	list_add_tail(&file->list, &contents_list); 
	d_instantiate(dentry, inode);  
	dget(dentry);

	return 0;
cleanup:
	iput(inode);
	kfree(file);
	return -EINVAL;
}


static const struct inode_operations compfs_dir_inode_operations = {
        .create         = compfs_create,
	.lookup		= simple_lookup,
};


static int compfs_fill_super(struct super_block *sb, void *data, int silent)
{
        struct inode * inode;
        struct dentry * root;

        sb->s_maxbytes = 4096;
	/* Cafe eh uma bebida saldavel, mas nao exagere. */
        sb->s_magic = 0xBEBACAFE;
	sb->s_blocksize = 1024;
	sb->s_blocksize_bits = 10;

        sb->s_op = &compfs_ops;
        sb->s_time_gran = 1;

        inode = new_inode(sb);

        if (!inode)
                return -ENOMEM;

        inode->i_ino = ++inode_number;
        inode->i_mtime = inode->i_atime = inode->i_ctime = CURRENT_TIME;
        inode->i_blocks = 0;
        inode->i_uid = inode->i_gid = 0;
        inode->i_mode = S_IFDIR | S_IRUGO | S_IXUGO | S_IWUSR;
        inode->i_op = &compfs_dir_inode_operations;
        inode->i_fop = &simple_dir_operations;
        set_nlink(inode, 2);

        root = d_make_root(inode);
        if (!root) {
                iput(inode);
                return -ENOMEM;
        }
        sb->s_root = root;
        return 0;

}

static struct dentry *compfs_get_sb(struct file_system_type *fs_type, int flags, const char *dev_name,
		   void *data)
{
  return mount_bdev(fs_type, flags, dev_name, data, compfs_fill_super);
}

static struct file_system_type compfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= "crccompfs",
	.mount		= compfs_get_sb,
	.kill_sb	= kill_litter_super,
};

static int __init init_compfs_fs(void)
{
	INIT_LIST_HEAD(&contents_list);
	return register_filesystem(&compfs_fs_type);
}

static void __exit exit_compfs_fs(void)
{
	unregister_filesystem(&compfs_fs_type);
}

module_init(init_compfs_fs)
module_exit(exit_compfs_fs)
MODULE_LICENSE("GPL");
